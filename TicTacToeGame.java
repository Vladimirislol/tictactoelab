import java.util.Scanner;
public class TicTacToeGame {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Hello user! welcome to tic tac toe. let's start a game!");
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		String currentBoard = "";
		System.out.println(board);
		System.out.println("this program takes coordinates from 0 - 2, where 0 is the first row / column and 2 is the last row / column.");
		do {
			if (player == 1) {
				playerToken = Square.X;
			} else {
				playerToken = Square.O;
			}
			int row = 0;
			int col = 0;
			System.out.println("It is player "+player+"'s turn! input which row you want your token placed:");
			row = sc.nextInt();
			System.out.println("Now input which column you want your token placed:");
			col = sc.nextInt();
			while (!board.placeToken(row, col, playerToken)){
				System.out.println("that isn't a valid place to put your token!");
				System.out.println("It is player "+player+"'s turn! input which row you want your token placed:");
				row = sc.nextInt();
				System.out.println("Now input which column you want your token placed:");
				col = sc.nextInt();
			} 
			System.out.println(board);
			if (board.checkIfWinning(playerToken)) {
				System.out.println("The game is over! player "+player+" won!");
				gameOver = true;
			} else if (board.checkIfFull()) {
				System.out.println("The board has been filled! it's a tie!");
				gameOver = true;
			} else {
				player ++;
				if (player > 2) {
					player = 1;
				}
			}
		} while (!gameOver);
	}
}