public class Board {
	private Square[][] board;
	public Board() {
		board = new Square[3][3];
		
		for (int i=0; i < this.board.length; i++) {
			for (int j =0; j < board[i].length; j++) {
				board[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString() {
		String currentBoard = "";
		for (int i=0; i < this.board.length; i++) {
				for (int j = 0; j < this.board[i].length; j++) {
					currentBoard += this.board[i][j];
				}
				currentBoard += "\n";
		}
		return currentBoard;
	}
	public boolean placeToken(int row, int col, Square playerToken) {
		boolean ok = false;
		if (row > this.board[0].length || row < 0 || col > this.board.length || col < 0) {
			ok = false; 
		}
		if (this.board[row][col] == Square.BLANK) {
			this.board[row][col] = playerToken;
			System.out.println("token placed");
			ok = true;
		} 
		return ok;
	}
	public boolean checkIfFull() {
		for (int i=0; i < this.board.length; i++) {
			for (int j =0; j < this.board[i].length; j++) {
				if (this.board[i][j] == Square.BLANK) {
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Square playerToken) {
		for (int i = 0; i < this.board.length; i++) {
			int counter = 0;
			for (int y = 0; y < this.board[i].length; y++) {
				if (this.board[i][y] == playerToken){
					counter ++;
				}
			}
			if (counter == 3) {
				return true;
			}
		}
		return false;
	}
	private boolean checkIfWinningVertical(Square playerToken) {
		for (int i = 0; i < this.board.length; i++) {
			int counter = 0;
			for (int y = 0; y < this.board[i].length; y++) {
				if (this.board[y][i] == playerToken){
					counter ++;
				}
			}
			if (counter == 3) {
				return true;
			}
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken) {
		boolean winning = false;
		if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
			winning = true;
		}
		return winning;
	}
	
}